package presentation;

import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import dao.ClientDAO;
import dao.ProductDAO;
import dao.OrderDAO;
import model.Client;
import model.Product;
import model.Orders;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class First{
    
	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JPanel panelClient;
	private JPanel panelProd;
	private JPanel panelStart;
	private JButton btnClient;
	private JButton btnProduct;
	private JButton btnReturn;
	private JButton btnInsert;
	private JButton btnDelete;
	private JButton btnUpdate;
	private JButton btnInsertProd;
	private JButton btnUpdateProd;
	private JButton btnDeleteProd;
	private JButton btnFind_1;
	private JButton btnShowProducts;
	private JTable table;
	private JTextField textField_ClientID;
	private JTextField textField_7;
	private JTable table_1;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private static String filename="E:\\receipt.txt";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
					First window = new First();
					window.frame.setVisible(true);
				
		
	}

	/**
	 * Create the application.
	 */
	public First() {
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 555, 399);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
	    JPanel panelStart = new JPanel();
		frame.getContentPane().add(panelStart, "name_586332008121322");
		panelStart.setLayout(null);
		panelStart.setVisible(true);
		
		 JPanel panelClient = new JPanel();
			frame.getContentPane().add(panelClient, "name_586335308900019");
			panelClient.setLayout(null);
			panelClient.setVisible(false);
			
			 JPanel panelProd = new JPanel();
				frame.getContentPane().add(panelProd, "name_586337080291867");
				panelProd.setLayout(null);
				panelProd.setVisible(false);
		
		JButton btnClient = new JButton("Client");
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelStart.setVisible(false);
				panelClient.setVisible(true);
			}
		});
		btnClient.setBounds(12, 70, 131, 55);
		panelStart.add(btnClient);
		
		JButton btnProduct = new JButton("Product");
		btnProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelStart.setVisible(false);
				panelProd.setVisible(true);
			}
		});
		btnProduct.setBounds(394, 70, 131, 55);
		panelStart.add(btnProduct);
		
		JLabel lblMeniu = new JLabel("Meniu");
		lblMeniu.setBounds(245, 42, 123, 16);
		panelStart.add(lblMeniu);
		
		JLabel lblMakeAnOrder = new JLabel("Make an Order:");
		lblMakeAnOrder.setBounds(29, 164, 178, 16);
		panelStart.add(lblMakeAnOrder);
		
		textField_9 = new JTextField();
		textField_9.setBounds(180, 193, 116, 22);
		panelStart.add(textField_9);
		textField_9.setColumns(10);
		
		textField_10 = new JTextField();
		textField_10.setBounds(180, 238, 116, 22);
		panelStart.add(textField_10);
		textField_10.setColumns(10);
		
		JLabel lblClientid = new JLabel("ClientID");
		lblClientid.setBounds(39, 193, 56, 16);
		panelStart.add(lblClientid);
		
		JLabel lblProductid = new JLabel("ProductID");
		lblProductid.setBounds(39, 241, 56, 16);
		panelStart.add(lblProductid);
		
		JLabel lblQuantity_1 = new JLabel("Quantity");
		lblQuantity_1.setBounds(39, 284, 56, 16);
		panelStart.add(lblQuantity_1);
		
		textField_11 = new JTextField();
		textField_11.setBounds(180, 281, 116, 22);
		panelStart.add(textField_11);
		textField_11.setColumns(10);
		
		JButton btnMakeOrder = new JButton("Make Order");
		btnMakeOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int clients=Integer.parseInt(textField_9.getText());
				int products=Integer.parseInt(textField_10.getText());
				int q=Integer.parseInt(textField_11.getText());
				OrderDAO dao=new OrderDAO();
				ProductDAO da=new ProductDAO();
				Product p=da.findById(products);
				p.setStore(p.getStore()-q);
				da.update(p, p.getId());
				Orders o=new Orders(clients,products,q);
				dao.insert(o);
				
				
			}
		});
		btnMakeOrder.setBounds(361, 192, 142, 25);
		panelStart.add(btnMakeOrder);
		
		JButton btnMakeReceipt = new JButton("Make Receipt");
		btnMakeReceipt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int clients=Integer.parseInt(textField_9.getText());
				int products=Integer.parseInt(textField_10.getText());
				int q=Integer.parseInt(textField_11.getText());
				BufferedWriter bw = null;
				FileWriter fw = null;
				
				try {

					String content;
                    ClientDAO dao=new ClientDAO();
                    Client c=dao.findById(clients);
					fw = new FileWriter(filename);
					bw = new BufferedWriter(fw);
					content=c.getName() + "  " +c.getAddress()+"  "+c.getEmail()+ "\n";
					bw.write(content);
					bw.newLine();
					ProductDAO da=new ProductDAO();
					Product p=da.findById(products);
					content="Bought "+q+" "+p.getName()+" which costs: "+p.getPrice()+"\n";
					bw.write(content);
					bw.newLine();
					content="These items totally  cost: "+p.getPrice()*q;
					bw.write(content);
					bw.newLine();
					bw.close();
					fw.close();
					

				} catch (IOException ex) {

					ex.printStackTrace();
				}
				
				
				
			}
		});
		btnMakeReceipt.setBounds(361, 280, 142, 25);
		panelStart.add(btnMakeReceipt);
		
		
		
		textField = new JTextField();
		textField.setBounds(67, 196, 116, 22);
		panelClient.add(textField);
		textField.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(12, 199, 56, 16);
		panelClient.add(lblName);
		
		JLabel lblAge = new JLabel("Age");
		lblAge.setBounds(12, 228, 56, 16);
		panelClient.add(lblAge);
		
		textField_1 = new JTextField();
		textField_1.setBounds(67, 228, 116, 22);
		panelClient.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(67, 254, 116, 22);
		panelClient.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(12, 257, 56, 16);
		panelClient.add(lblAddress);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(12, 286, 56, 16);
		panelClient.add(lblEmail);
		
		textField_3 = new JTextField();
		textField_3.setBounds(67, 283, 116, 22);
		panelClient.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelClient.setVisible(false);
				panelStart.setVisible(true);
			}
		});
		btnReturn.setBounds(427, 299, 97, 25);
		panelClient.add(btnReturn);
		
		table=new JTable();
		table.setBounds(12, 17, 512, 155);
		panelClient.add(table);
        DefaultTableModel model = new DefaultTableModel();
        Object[] columnsName = new Object[5];
        columnsName[0] = "ClientID";
        columnsName[1] = "name";
        columnsName[2] = "age";
        columnsName[3] = "address";
        columnsName[4] = "email";
        model.setColumnIdentifiers(columnsName);
        Object[] rowData = new Object[5];
        ClientDAO dao=new ClientDAO();
        for(int i = 0; i < dao.getClients().size(); i++){
        	 rowData[0] = dao.getClients().get(i).getId();
        	 rowData[1] = dao.getClients().get(i).getName();
        	 rowData[2] = dao.getClients().get(i).getAge();
        	 rowData[3] = dao.getClients().get(i).getAddress();
        	 rowData[4] = dao.getClients().get(i).getEmail();
        	 model.addRow(rowData);
        	 
        }
        table.setModel(model);
        
        JButton btnInsert = new JButton("Insert");
        btnInsert.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0)
        	{
        		ClientDAO dao=new ClientDAO();
        		String name=textField.getText();
        		int age=Integer.parseInt(textField_1.getText());
        		String address=textField_2.getText();
        		String email=textField_3.getText();
        		Client c=new Client(name,address,email,age);
        		dao.insert(c);
        		
        		
        		
        	}
        });
        btnInsert.setBounds(77, 312, 97, 25);
        panelClient.add(btnInsert);
        
        JLabel lblId = new JLabel("Id");
        lblId.setBounds(244, 199, 56, 16);
        panelClient.add(lblId);
        
        textField_ClientID = new JTextField();
        textField_ClientID.setBounds(282, 196, 116, 22);
        panelClient.add(textField_ClientID);
        textField_ClientID.setColumns(10);
        
        JButton btnFind = new JButton("FindByID");
        btnFind.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		try {
        		ClientDAO dao=new ClientDAO();
        		System.out.println(dao.findById(Integer.parseInt(textField_ClientID.getText())));
        		}
        		catch (NumberFormatException e)
        		{
        			JOptionPane.showMessageDialog(null, "Id is incorrect!");
        		}
        	}
        });
        btnFind.setBounds(282, 228, 97, 25);
        panelClient.add(btnFind);
        
        JButton btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0)
        	{
        		ClientDAO dao=new ClientDAO();
        		int id=Integer.parseInt(textField_ClientID.getText());
        		dao.delete(id);
        		
        		
        		
        	}
        });
        btnDelete.setBounds(282, 266, 97, 25);
        panelClient.add(btnDelete);
        
        JButton btnUpdate = new JButton("Update");
        btnUpdate.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0)
        	{
        		try
        		{
        		ClientDAO dao=new ClientDAO();
        		int id=Integer.parseInt(textField_ClientID.getText());
        		String name=textField.getText();
        		int age=Integer.parseInt(textField_1.getText());
        		String address=textField_2.getText();
        		String email=textField_3.getText();
        		Client c=new Client(name,address,email,age);
        		dao.update(c, id);
        		}
        		catch(NumberFormatException e)
        		{
        			JOptionPane.showMessageDialog(null, "Id is incorrect!");
        		}
        		
        		
        	}
        });
        btnUpdate.setBounds(282, 299, 97, 25);
        panelClient.add(btnUpdate);
        
        JButton btnShowClients = new JButton("List");
        btnShowClients.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		 DefaultTableModel model = new DefaultTableModel();
        	        Object[] columnsName = new Object[5];
        	        columnsName[0] = "ClientID";
        	        columnsName[1] = "name";
        	        columnsName[2] = "age";
        	        columnsName[3] = "address";
        	        columnsName[4] = "email";
        	        model.setColumnIdentifiers(columnsName);
        	        Object[] rowData = new Object[5];
        	        ClientDAO dao=new ClientDAO();
        	        for(int i = 0; i < dao.getClients().size(); i++){
        	        	 rowData[0] = dao.getClients().get(i).getId();
        	        	 rowData[1] = dao.getClients().get(i).getName();
        	        	 rowData[2] = dao.getClients().get(i).getAge();
        	        	 rowData[3] = dao.getClients().get(i).getAddress();
        	        	 rowData[4] = dao.getClients().get(i).getEmail();
        	        	 model.addRow(rowData);
        	        	 
        	        }
        	        table.setModel(model);
        		
        	}
        });
        btnShowClients.setBounds(427, 224, 97, 25);
        panelClient.add(btnShowClients);
      
		
		
		
		JLabel lblName_1 = new JLabel("Name");
		lblName_1.setBounds(357, 195, 56, 16);
		panelProd.add(lblName_1);
		
		textField_4 = new JTextField();
		textField_4.setBounds(420, 192, 116, 22);
		panelProd.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(357, 230, 56, 16);
		panelProd.add(lblPrice);
		
		textField_5 = new JTextField();
		textField_5.setBounds(420, 224, 116, 22);
		panelProd.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("BrandID");
		lblNewLabel.setBounds(357, 265, 56, 16);
		panelProd.add(lblNewLabel);
		
		textField_6 = new JTextField();
		textField_6.setBounds(420, 259, 116, 22);
		panelProd.add(textField_6);
		textField_6.setColumns(10);
		
		JButton btnReturn_1 = new JButton("Return");
		btnReturn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelProd.setVisible(false);
				panelStart.setVisible(true);
			}
		});
		btnReturn_1.setBounds(439, 327, 97, 25);
		panelProd.add(btnReturn_1);
		
		JButton btnInsertProd = new JButton("Insert");
		btnInsertProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=textField_4.getText();
				int price=Integer.parseInt(textField_5.getText());	
				int ProducerID=Integer.parseInt(textField_6.getText());
				int q=Integer.parseInt(textField_8.getText());
				Product p=new Product(name,price,q,ProducerID);
				System.out.println(p.toString());
				ProductDAO dao=new ProductDAO();
				dao.insert(p);
				
			}
		});
		btnInsertProd.setBounds(226, 208, 97, 25);
		panelProd.add(btnInsertProd);
		
		JButton btnDeleteProd = new JButton("Delete");
		btnDeleteProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(textField_7.getText());
				
				ProductDAO dao=new ProductDAO();
				dao.delete(id);
				
			}
		});
		btnDeleteProd.setBounds(226, 241, 97, 25);
		panelProd.add(btnDeleteProd);
		
		JButton btnUpdateProd = new JButton("Update");
		btnUpdateProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name=textField_4.getText();
				int price=Integer.parseInt(textField_5.getText());	
				int ProducerID=Integer.parseInt(textField_6.getText());
				int q=Integer.parseInt(textField_7.getText());
				Product p=new Product(name,price,q,ProducerID);
				int id=Integer.parseInt(textField_7.getText());
				ProductDAO dao=new ProductDAO();
				dao.update(p, id);
				
			}
		});
		btnUpdateProd.setBounds(226, 276, 97, 25);
		panelProd.add(btnUpdateProd);
		
		JButton btnFind_1 = new JButton("Find");
		btnFind_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(textField_7.getText());
				ProductDAO dao=new ProductDAO();
				System.out.println(dao.findById(id).toString());
				
			}
		});
		btnFind_1.setBounds(226, 312, 97, 25);
		panelProd.add(btnFind_1);
		
		textField_7 = new JTextField();
		textField_7.setBounds(85, 312, 116, 22);
		panelProd.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblId_1 = new JLabel("Id");
		lblId_1.setBounds(31, 316, 56, 16);
		panelProd.add(lblId_1);
		
		table_1 = new JTable();
		table_1.setBounds(0, 13, 536, 177);
		panelProd.add(table_1);
		
		JButton btnShowProducts = new JButton("List");
		btnShowProducts.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
       		 DefaultTableModel model = new DefaultTableModel();
       	        Object[] columnsName = new Object[5];
       	        columnsName[0] = "id";
       	        columnsName[1] = "name";
       	        columnsName[2] = "price";
       	        columnsName[3] = "store";
       	        columnsName[4] = "Producer";
       	        
       	        model.setColumnIdentifiers(columnsName);
       	        Object[] rowData = new Object[5];
       	        ProductDAO dao=new ProductDAO();
       	        for(int i = 0; i < dao.getProducts().size(); i++){
       	        	 rowData[0] = dao.getProducts().get(i).getId();
       	        	 rowData[1] = dao.getProducts().get(i).getName();
       	        	 rowData[2] = dao.getProducts().get(i).getPrice();
       	        	 rowData[3]= dao.getProducts().get(i).getStore();
       	        	 rowData[4] = dao.getProducts().get(i).getProducer();
       	        	
       	        	 model.addRow(rowData);
       	        	 
       	        }
       	        table_1.setModel(model);
       		
       	}
       });
		btnShowProducts.setBounds(82, 208, 97, 25);
		panelProd.add(btnShowProducts);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(357, 294, 56, 16);
		panelProd.add(lblQuantity);
		
		textField_8 = new JTextField();
		textField_8.setBounds(420, 291, 116, 22);
		panelProd.add(textField_8);
		textField_8.setColumns(10);
	}
}

