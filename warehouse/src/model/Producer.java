package model;

public class Producer {
	
	private int producerID;
	private int year;
	private String officeAdd;
	private String name;
	
	public Producer(int id,int year,String office)
	{
		super();
		this.producerID=id;
		this.year=year;
		this.officeAdd=office;
	}
	
	public Producer(int year,String office)
	{
		super();
		this.year=year;
		this.officeAdd=office;
	}
	
	public int getID(){
		return this.producerID;
	}
	
	public void setID(int id)
	{
		this.producerID=id;
	}
	
	public int getYear()
	{
		return this.year;
	}
	
	public void setYear(int year)
	{
		this.year=year;
	}
	
	public void setAdd(String add)
	{
		this.officeAdd=add;
	}
	
	public String getAdd()
	{
		return this.officeAdd;
	}
	
	public String getName()
	{
	  return this.name;
	}
	
	public void setName(String name)
	{
		this.name=name;
	}
}
