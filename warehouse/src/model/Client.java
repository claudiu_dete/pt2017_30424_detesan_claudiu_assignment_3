package model;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Client {
	private int id; 
	private String name;
	private int age;
	private String Address;
	private String email;

	public Client(int ClientID, String name, int age, String address, String email) {
		super();
		this.id = ClientID;
		this.name = name;
		this.age = age;
		this.Address = address;
		this.email = email;
	}

	public Client(String name, String address, String email, int age) {
		super();
		this.name = name;
		this.age = age;
		this.Address = address;
		this.email = email;
	}
	
	public Client()
	{
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		this.Address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", address=" + Address + ", email=" + email + ", age=" + age
				+ "]";
	}

}
