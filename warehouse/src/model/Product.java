package model;

public class Product {
	
	private int id;
	private String name;
	private double price;
	private int store;
	private int Producer;
	
	public Product(int ProductID, String name, double price,int store, int ProducerID) {
		super();
		this.id = ProductID;
		this.name = name;
		this.price = price;
		this.store=store;
		this.Producer = ProducerID;
		
	}

	public Product(String name, double price,int store, int ProducerID) {
		super();
		this.name = name;
		this.price = price;
		this.store=store;
		this.Producer = ProducerID;
		
	}
	public Product()
	{
		
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice()
	{
		return this.price;
	}
	
	public void setPrice(double p)
	{
		this.price=p;
	}
	
	public int getStore()
	{
		return this.store;
	}
	
	public void setStore(int store)
	{
		this.store=store;
	}
	
	public int getProducer()
	{
		return this.Producer;
	}
	
	public void setProducer(int id)
	{
		this.Producer=id;
	}
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price+ ", ProducerID=" + Producer   
				+ "]"+"store="+store;
	}
	
	

}
