package model;



public class Orders {
	private int id;
	private int cl;
	private int pr;
	private int q;
	
	public Orders(int id,int client,int product,int quantity)
	{
        this.id=id;
		this.cl=client;
		this.pr=product;
		this.q=quantity;
	}
	public Orders(int clients,int products,int quantity)
	{
		
		this.cl=clients;
		this.pr=products;
		this.q=quantity;
	}
	
	public Orders()
	{
		
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public int getCl()
	{
		return this.cl;
	}
	
	public void setCl(int id)

	{
		this.cl=id;
	}
	
	public int getPr()
	{
		return this.pr;
	}
	
	public void setPr(int id)
	{
		this.pr=id;
	}
	
	public void setQ(int q)
	{
		this.q=q;
	}
	
	public int getQ()
	{
		return this.q;
	}
}