package dao;


import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import connection.ConnectionFactory;




public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private final Class<T> type;
    @SuppressWarnings("unchecked")
    public AbstractDAO(){
        this.type=(Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    public static String capitalize(String s) {
        
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }
    
    public static List<Field> getFields(Object obj){
        List<Field> fieldList=new ArrayList<>();
        fieldList.addAll(Arrays.asList(obj.getClass().getDeclaredFields()));
        return fieldList;
    }

    public String createSelectStatment(String field){
        StringBuilder sb=new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE "+field+" =?");
        return sb.toString();
    }
    public String createInsertStatment(T t){
        List<Field>fieldList;
        int i=1;
        fieldList=getFields(t);
        StringBuilder sb=new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName());
        sb.append( " (");
        for(Field f:fieldList) {
            if (!(f.getName().equals("id"))) { //for auto incremented ids
                if (i < fieldList.size()-1) {
                    sb.append(f.getName() + ",");
                } else {
                    sb.append(f.getName());
                }
                
                i++;
            }
        }
         sb.append(") ");
         sb.append("VALUES ");
        sb.append("(");
        for( i=0;i<fieldList.size()-1;i++){
            if(i+1<fieldList.size()-1){
                sb.append("?,");
            }
            
                
            

        }
        sb.append("?");
        sb.append(")");
   
        return sb.toString();
    }

    public String createUpdateStatment(T t){
        List<Field>fieldList;
        int current=1;
        fieldList=getFields(t);
        StringBuilder sb=new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append( " SET ");
        for(Field f:fieldList) {
            if (!(f.getName().equals("id"))) {  //for autoincremented ids
                if (current < fieldList.size()-1) {
                    sb.append(f.getName() + " = ?,");
                } else {
                    sb.append(f.getName()+" = ?");
                }
                current++;
            }
        }
        sb.append(" WHERE id=?");

        return sb.toString();
    }

    public String createDeleteStatment(){
        StringBuilder sb=new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE id=?");
        return sb.toString();
    }



    public void insert(T t){
        
        Connection con=ConnectionFactory.getConnection();
        PreparedStatement st=null;
        ResultSet generatedKeys=null;
        String query=createInsertStatment(t);
        List<Field> fields=getFields(t);
        try{
            st=con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i=1;
            for(Field f:fields){
                if(!(f.getName().equals("id"))) {
                    try {
                        Method method = t.getClass().getMethod("get" + capitalize(f.getName()));
                        
                        st.setObject(i, method.invoke(t));
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SQLException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            }
            System.out.println(st.toString());
            st.executeUpdate();
            generatedKeys=st.getGeneratedKeys();
            if(generatedKeys.next()){
                

            }

        }
        catch(SQLException e){
           
        }
        finally {
            ConnectionFactory.close(con);
            ConnectionFactory.close(generatedKeys);
            ConnectionFactory.close(st);
        }
        return;
    }
  private List<T> createObjects(ResultSet resultSet){
      List<T> list=new ArrayList<T>();
      try{
      while(resultSet.next()){
          T instance=type.newInstance();
          for(Field field:type.getDeclaredFields()){
              Object value=resultSet.getObject(field.getName());
              PropertyDescriptor propertyDescriptor=new PropertyDescriptor(field.getName(),type);
              Method method=propertyDescriptor.getWriteMethod();
              method.invoke(instance,value);
          }
          list.add(instance);
      }
      }
      catch(IntrospectionException|IllegalAccessException|InvocationTargetException|SQLException|InstantiationException e){
          e.printStackTrace();
      }
      return list;
  }

    public T findById(int id){
        Connection connection =null;
        PreparedStatement statement=null;
        ResultSet results=null;
        String query = createSelectStatment("id");
        try{
            connection=ConnectionFactory.getConnection();
            statement=connection.prepareStatement(query);
            statement.setInt(1,id);
            results=statement.executeQuery();
            return createObjects(results).get(0);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(results);
            ConnectionFactory.close(statement);

        }
        return null;
    }

    public void update(T t,int id){
        Connection con=ConnectionFactory.getConnection();
        PreparedStatement st=null;
        ResultSet generatedKeys=null;
        String query=createUpdateStatment(t);
        List<Field> fields=getFields(t);
        try{
            st=con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            int i=1;
            for(Field f:fields){
                if(!(f.getName().equals("id"))) {
                    try {
                        Method method = t.getClass().getMethod("get" + capitalize(f.getName()));
                        st.setObject(i, method.invoke(t));
                    } catch ( InvocationTargetException | NoSuchMethodException | SQLException | IllegalAccessException | IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            }
            st.setInt(5,id);

            st.executeUpdate();
            generatedKeys=st.getGeneratedKeys();
         

        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(con);
            ConnectionFactory.close(generatedKeys);
            ConnectionFactory.close(st);
        }
       
    }

    public void delete(int id){
        Connection con=null;
        PreparedStatement st=null;
        String query=createDeleteStatment();
        try{
            con=ConnectionFactory.getConnection();
            st=con.prepareStatement(query);
            st.setInt(1,id);
            st.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(con);
            ConnectionFactory.close(st);
        }


    }





}
