package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;

import connection.ConnectionFactory;

import model.Product;

public class ProductDAO extends AbstractDAO<Product> {
	
	public  static ArrayList<Product>  getProducts()
	{
		ArrayList<Product> products=new ArrayList<Product>();
		Connection con=ConnectionFactory.getConnection();
		 Statement st=null;
	     ResultSet rs=null;
	     Product p;
	     try
	     {
	    	 st = con.createStatement();
	         rs = st.executeQuery("SELECT * FROM product");
	         while(rs.next()){
	        	 p=new Product(rs.getInt("id"),rs.getString("name"),rs.getInt("price"),rs.getInt("store"),rs.getInt("Producer"));
	        	 products.add(p);
	         }
	         
	         
	    	 
	     }
	     catch (SQLException e) {
	    	 LOGGER.log(Level.WARNING, "ProductDAO:getProducts " + e.getMessage());
	     }
	     
	     finally
	     {
	    	    ConnectionFactory.close(con);
	            ConnectionFactory.close(rs);
	            ConnectionFactory.close(st);
	     }
	        
	        
		return products;
		
	}

}
