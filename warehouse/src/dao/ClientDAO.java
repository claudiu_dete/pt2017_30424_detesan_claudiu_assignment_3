package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import model.Client;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ClientDAO extends AbstractDAO<Client> {

	public  static ArrayList<Client>  getClients()
	{
		ArrayList<Client> clients=new ArrayList<Client>();
		Connection con=ConnectionFactory.getConnection();
		 Statement st=null;
	     ResultSet rs=null;
	     Client c;
	     try
	     {
	    	 st = con.createStatement();
	         rs = st.executeQuery("SELECT * FROM client");
	         while(rs.next()){
	        	 c=new Client(rs.getInt("id"),rs.getString("name"),rs.getInt("age"),rs.getString("address"),rs.getString("email"));
	        	 clients.add(c);
	         }
	         
	         
	    	 
	     }
	     catch (SQLException e) {
	    	 LOGGER.log(Level.WARNING, "ClientDAO:getClients " + e.getMessage());
	     }
	     
	     finally
	     {
	    	    ConnectionFactory.close(con);
	            ConnectionFactory.close(rs);
	            ConnectionFactory.close(st);
	     }
	        
	        
		return clients;
		
	}
	public  JTable createTable(ArrayList<Client> clients)
	{
		 JTable table = new JTable();
		
		
        DefaultTableModel model = new DefaultTableModel();
        Object[] columnsName = new Object[5];
        columnsName[0] = "ClientID";
        columnsName[1] = "name";
        columnsName[2] = "age";
        columnsName[3] = "address";
        columnsName[4] = "email";
        model.setColumnIdentifiers(columnsName);
        Object[] rowData = new Object[5];
        
        for(int i = 0; i < getClients().size(); i++){
        	 rowData[0] = getClients().get(i).getId();
        	 rowData[1] = getClients().get(i).getName();
        	 rowData[2] = getClients().get(i).getAge();
        	 rowData[3] = getClients().get(i).getAddress();
        	 rowData[4] = getClients().get(i).getEmail();
        	 model.addRow(rowData);
        	 
        }
        table.setModel(model);
        return table;
	}
	
}
